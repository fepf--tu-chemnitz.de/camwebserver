#!/usr/bin/env python3

from server import InloopImgServer
from datetime import datetime
import cv2
import numpy as np
from pathlib import Path

cs = [(i, cv2.VideoCapture(str(i))) for i in Path("/dev/").glob("video*")]
cs = list((i, c) for i, c in cs if c.isOpened())

s = InloopImgServer(title="cam web server", nimgs=len(cs))

def capture_frame(cam):
    res, img = cam.read()
    if img is None:
        img = np.zeros((600, 800, 3), "uint8")
        return cv2.putText(
                     img
                    ,f"could not read from device"
                    ,(10, img.shape[0] - 30)
                    ,cv2.FONT_HERSHEY_PLAIN
                    ,1
                    ,(255, 0, 0)
                )
    return img

def annotate(camIdx, img):
    return cv2.putText( 
                     img
                    ,f"camera {camIdx} {datetime.now().isoformat()}"
                    ,(10,img.shape[0] - 10)
                    ,cv2.FONT_HERSHEY_PLAIN
                    ,1
                    ,(255,)*3
                      )

while True:
    s.img = list(annotate(i, capture_frame(c)) for i, c in cs)

s.stop()
