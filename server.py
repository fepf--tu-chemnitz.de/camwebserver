from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer, HTTPServer
import cv2
import numpy as np
from PIL import Image
from io import BytesIO
import time, re
from contextlib import suppress
from threading import Thread


CAMPATTERN = re.compile(r".*cam\.(?P<idx>\d+)\.jpg$")

class HTMLTag():
    def __init__(self, tagname=None, attrs={}):
        self._tn = tagname
        self._attrs = attrs or {}

    def attrs(self, **attrs):
        return HTMLTag(self._tn, attrs)

    a = attrs

    @property
    def xml_attrs(self):
        return " " + " ".join(f"{k}='{v}'" for k, v in self._attrs.items()) if self._attrs else ""

    def _render_content(self, content):
        return "\n".join(f"{c()}" if callable(c) else f"{c}" for c in content)

    def __call__(self, *content):
        return f"<{self._tn}{self.xml_attrs}>{self._render_content(content)}</{self._tn}>"

HT = HTMLTag
DIV = HT("DIV")
IMG = HT("IMG")
HTML = HT("HTML")
HEAD = HT("HEAD")
BODY = HT("BODY")
SCRIPT = HT("SCRIPT")
TITLE = HT("TITLE")

_noise_img = lambda: np.random.randint(0, 255, (480, 640, 3), dtype="uint8")

def mk_ImgHandler(title="img server", nimgs=1, get_img=_noise_img):
    class ImgHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(200)

            if self.path.endswith('.jpg'): return self.send_mjpg()
            return self.send_page(nimgs)


        def send_page(self, nimgs):
            self.send_header('Content-Type', "text/html")
            self.end_headers()
            resp = HTML(
                     HEAD(TITLE(title))
                    ,BODY(*[IMG.a(src=f"cam.{i}.jpg", idx=i, Class="cam")() for i in range(nimgs)])
                    )
            self.wfile.write(bytes(resp, "utf8"))


        def send_mjpg(self):
            m = CAMPATTERN.match(self.path)
            camidx = int(m.group("idx")) if m else 0

            self.send_header("Content-Type", "multipart/x-mixed-replace; boundary=--jpgboundary")
            self.send_header("Cache-Control", "max-age=0, no-cache, must-revalidate, no-store")
            self.end_headers() 

            with suppress(BrokenPipeError):
                while True:
                    jpeg = BytesIO()
                    Image.fromarray(get_img(camidx)).save(jpeg, "JPEG")
                    jpeg.seek(0)
                    buf = jpeg.read()

                    self.wfile.write(b"--jpgboundary\n")
                    self.send_header('Content-Type', "image/jpeg")
                    self.send_header("Content-Length", f"{len(buf)}")
                    self.end_headers()

                    self.wfile.write(buf)
                    self.wfile.flush()
                    time.sleep(0.05)

    return ImgHandler

class InloopImgServer:
    def __init__(self, title="img server", host=('0.0.0.0', 8080), start_server=True, get_img_fn=None, nimgs=1):
        self.img = np.zeros((640, 480, 3), dtype=np.uint8)
        self.server = ThreadingHTTPServer(host, mk_ImgHandler(title, get_img=get_img_fn or self._parse_img, nimgs=nimgs))
        if start_server:
            self.start()

    def _parse_img(self, idx=0):
        return self.img[idx] if type(self.img) in (list, tuple) else self.img

    def start(self):
        self._t = Thread(target=self.server.serve_forever)
        self._t.start()

    def stop(self):
        self.server.server_shutdown()
        self._t.join()

if __name__ == "__main__":
    s = InloopImgServer()
    while True:
        s.img = _noise_img()
        time.sleep(0.1)
