{
  description = "A Simple MJPG webcam webserver";
  inputs = {
    nixpkgs.url = "nixpkgs";
  };
  outputs = { self, nixpkgs }: 
  with nixpkgs;
  let
    forAllSystems = f: lib.genAttrs lib.platforms.all f;
    deps = [];
    fpypkgs = pps: with pps; [pillow opencv4];
    thispython = s: nixpkgs.legacyPackages.${s}.python3;
  in
  rec {

    packages = forAllSystems (s: 
      with nixpkgs.legacyPackages.${s};
      { 
      camwebserver = stdenv.mkDerivation {
        name = "camwebserver";
        version = "0.1";
        installPhase = ''
          mkdir -p $out/bin
          cp *.py $out/bin
          cp camwebserver $out/bin
          chmod +x $out/bin/camwebserver
          '';
        buildInputs = deps ++ [((thispython s).withPackages fpypkgs)];
        src = ./.;
      };
    });
    defaultPackage = forAllSystems (s: packages.${s}.camwebserver );

    devShells = forAllSystems (s: 
      with nixpkgs.legacyPackages.${s};
      { 
      default = mkShell {
        buildInputs = deps ++ [
          neovim 
          (
            (thispython s).withPackages (pps: (fpypkgs pps) ++ (with pps; [bpython] ))
          )
          ];
        };
      });

  };
}
